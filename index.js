const readline = require('readline');
const fs = require('fs');
let employee = require('./data.json');

//Delete function
const delfun = (name,val)=> {
  prompt.question('Are you sure ? :',(ans)=>{
    if(ans=='yes'|| ans=='Yes'|| ans=='Y'|| ans=='y' || ans=='YES'){
      for(let i=0; i < employee.length;i++){
        if(employee[i].userName===name){
        employee.splice(i, 1);
        for(let j=0; j < employee.length;j++){
          employee[j].userid=j+1;
        }
        fs.writeFile('./data.json',JSON.stringify(employee),(err) => {
            prompt.question('Do you want to Exit ? : ',(ans)=>{
              if(ans=='yes'|| ans=='Yes'|| ans=='Y'|| ans=='y' || ans=='YES'){
                process.exit();
              }
              else{
                ques();
                mainFun();
              };
            });
          });
        }
      }
    }
    else {
      ques();
      mainFun();
    };
  });
}
//Options for users
const ques = () =>{
  console.log('1.Get all Employee.');
  console.log('2.Add an Employee.');
  console.log('3.Delete an Employee.');
  console.log('4.Do you want to Exit.');
}

//Get user input
const prompt = readline.createInterface(process.stdin,process.stdout);
ques();
let mainFun =() => {  prompt.question('Select any one of the above by appropriate indexNo:',(userip) => {
  //Switch cases according to the user input
  let ans = parseInt(userip);
  switch (ans) {
    case 1 :
      // display all employee details
      for(let i=0;i < employee.length;i++){
        console.log(`Employee Name: ${employee[i].userName}`);
        console.log(`Employee Id: ${employee[i].userid}`);
        console.log(`Email: ${employee[i].email}`);
      }
      prompt.question('Do you want to Exit ? : ',(ans)=>{
        if(ans=='yes'|| ans=='Yes'|| ans=='Y'|| ans=='y' || ans=='YES'){
          process.exit();
        }
        else{
          ques();
          mainFun();
        };
      });
      break;
    // add new employee details
    case 2 :
      prompt.question('Employee Name : ',(name)=>{
        for(let i=0; i < employee.length;i++){
          if(employee[i].userName===name){
            console.warn('UserName already exits!')
            process.exit();
          }
        }
        prompt.question('Employee Email : ',(email)=>{
          let newEmployee = {
            "userName": name,
            "userid": employee.length+1,
            "email": email
          };
          employee.push(newEmployee);
          fs.writeFile('./data.json',JSON.stringify(employee),(err) => {
            prompt.question('Do you want to Exit ? : ',(ans)=>{
              if(ans=='yes'|| ans=='Yes'|| ans=='Y'|| ans=='y' || ans=='YES'){
                process.exit();
              }
              else{
                ques();
                mainFun();
              };
            });
          });
        });
      });
      break;
    //remove or delete the employee from the data
    case 3 :
      prompt.question('Employee Name : ',(name)=>{
        let promise = new Promise ((resolve,reject)=>{
          for(let a=0; a < employee.length; a++){
            if(employee[a].userName===name){
              resolve();
            }
          }
          for(let b=0; b < employee.length; b++){
            if(employee[b].userName!=name){
              reject(Error('Employee name not found'))
            }
          }
        })
        promise.then(()=>{
          let bool = true;
          delfun(name,bool); 
        },function(err) {
          console.log(err);
          ques();
          mainFun();
        });
      });
      break;
    //if user want to exit 
    case 4:
      process.exit();
    //if user entered wrong input
    default :
    console.error('Please enter an appropriate indexNo!');
    process.exit();
  };
});
};
mainFun();